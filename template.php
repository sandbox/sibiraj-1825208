<?php

/**
 * @file
 * Template overrides and (pre-)process hooks for the minimalism theme.
 */
	 
/**
 * Override of theme_breadcrumb().
 */
function minimalism_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
	$crumbs = '';
  if (!empty($breadcrumb)) {
    $breadcrumb[] = drupal_get_title();
    return '<div class="breadcrumb">'. implode('&nbsp;&rArr;&nbsp;', $breadcrumb) .'</div>';
  }
}

/**
 * Override or insert variables into the page template.
 */
function minimalisum_preprocess_page(&$vars) {
	if (isset($vars['main_menu'])) {
		$vars['primary_nav'] = theme('links__system_main_menu', array(
		'links' => $vars['main_menu'],
		'attributes' => array(
		'class' => array('links', 'inline', 'main-menu'),
		)
	));
	}
	else {
		$vars['primary_nav'] = FALSE;
	}
}

/**
 * Implements hook_theme().
 */
function minimalism_theme($existing, $type, $theme, $path) {
	return array(
		'front_block' => array(
		'variables' => array('node'=>NULL),
		'template' => 'templates/front-block'
		),
		'footer_block' => array(
		'variables' => array('node'=>NULL),
		'template' => 'templates/footer-block'
		),
	);
}

/**
 * function to add class for the layout settings.
 */
function minimalisum_load_layout($variables){
	if ($variables['sidebar_first'] && $variables['sidebar_second']) {
	  $class = 'two-side';
	} 
	elseif ($variables['sidebar_first']) {
	  $class = 'first-side';
	}
	elseif ($variables['sidebar_second']) {
	  $class = 'second-side';
	} 
	else {
	  $class = 'no-side';
	}
	return $class;
}

/**
 * Function implemented for optional theme settings in front page blocks.
 */
function minimalism_theme_front_block($nid) {
  $node = node_load($nid);
  return theme('front_block',array('node' => $node)); 
}

/**
 * Function implemented for optional theme settings in footer blocks.
 */
function minimalism_theme_footer_block($nid) {
  $node = node_load($nid);
  return theme('footer_block',array('node' => $node)); 
}