<div class="bg">
    <!--start container-->
    <div id="container" class="<?php echo minimalisum_load_layout($page); ?>">
    <!--start header-->
    <header>
      <!--start logo-->
       <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" width="180" height="43" />
        </a>
      <?php endif; ?>
      <!--end logo-->
      <!--start menu-->
  	   <nav>
          <?php if ($page['mainmenu']): print render($page['mainmenu']); ?>
           <?php else: ?>
					 <?php if ($primary_nav): print $primary_nav; endif; ?>
					 <?php endif; ?>
      </nav>
  	   <!--end menu-->
      <!--end header-->
	</header>
   <!--start intro-->
   <section id="intro">
      <hgroup>
        <?php if(theme_get_setting('intro_option') == '' || theme_get_setting('intro_option') == 0):?>
        	<?php print render($page['intro']); ?>
        <?php else: ?>
          <?php if(theme_get_setting('intro_option') == 2) :?>
            <h1><?php echo theme_get_setting('intro_title'); ?></h1>
            <h2><?php echo theme_get_setting('intro_desc'); ?></h2>
          <?php endif; ?>
        <?php endif; ?>
      </hgroup>
   </section>
   <!--end intro-->

    <?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>

    <?php print $messages; ?>
     <!--start holder--> 
     
     <div class="holder_content">
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if($page['sidebar_first']): ?>
          <div class="sidebar_left">
          	<?php print render($page['sidebar_first']); ?>
          </div>
        <?php endif; ?>
        
        <div class="content_main">
					<?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
          <?php print render($page['content']); ?>
          <?php print $feed_icons; ?>
        </div>
        
        <?php if($page['sidebar_second']): ?>
          <div class="sidebar_right">
          	<?php print render($page['sidebar_second']); ?>
          </div>
        <?php endif; ?>
      </div>

    <!--start holder-->
   <div class="holder_content">
      <h3>Latest article</h3>
      <section class="group4">
       <?php if(theme_get_setting('article1') != '' || theme_get_setting('article2') != '' || theme_get_setting('article3') != '') { ?>
					 <?php if(theme_get_setting('article1') != ''){
             echo minimalism_theme_footer_block(theme_get_setting('article1'));
           }?>
           <?php if(theme_get_setting('article2') != ''){
             echo minimalism_theme_footer_block(theme_get_setting('article2'));
           }?>
           <?php if(theme_get_setting('article3') != ''){
             echo minimalism_theme_footer_block(theme_get_setting('article3'));
           }?>
           
       <?php } else { ?>
          <?php print render($page['before_footer']); ?>
       <?php } ?>    
       </section>
   </div>
   <!--end holder-->
   </div>
   <!--end container-->
   <!--start footer-->
   <footer>
      <div class="container-footer">  
			   <?php print render($page['footer']); ?>
      </div>
   </footer>
   <!--end footer-->
   </div>