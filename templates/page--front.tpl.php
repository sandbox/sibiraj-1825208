<div class="bg">
    <!--start container-->
    <div id="container">
    <!--start header-->
    <header>
      <!--start logo-->
       <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" width="180" height="43" />
        </a>
      <?php endif; ?>
      <!--end logo-->
      <!--start menu-->
  	   <nav>
           <?php if ($page['mainmenu']): print render($page['mainmenu']); ?>
           <?php else: ?>
					 <?php if ($primary_nav): print $primary_nav; endif; ?>
					 <?php endif; ?>
      </nav>
  	   <!--end menu-->
      <!--end header-->
	</header>
   <!--start intro-->
   <section id="intro">
      <hgroup>
        <?php if(theme_get_setting('intro_option') == '' || theme_get_setting('intro_option') == 0):?>
        	<?php print render($page['intro']); ?>
        <?php else: ?>
          <h1><?php echo theme_get_setting('intro_title'); ?></h1>
      		<h2><?php echo theme_get_setting('intro_desc'); ?></h2>
        <?php endif; ?>
      </hgroup>
   </section>
   <!--end intro-->
   <?php print $messages; ?>
   <!--start holder-->
   <div class="holder_content">
       <section class="group1">
         <?php if(theme_get_setting('minimalism_group1') != '') { ?>
           <?php echo minimalism_theme_front_block(theme_get_setting('minimalism_group1')); ?>
         <?php } else { ?>
         	<?php print render($page['group1']); ?>
         <?php } ?>
       </section>
      <section class="group2">
         <?php if(theme_get_setting('minimalism_group2') != '') { ?>
           <?php echo minimalism_theme_front_block(theme_get_setting('minimalism_group2')); ?>
         <?php } else { ?>
           <?php print render($page['group2']); ?>
         <?php } ?>
       </section>
      <section class="group3">
        <?php if(theme_get_setting('minimalism_group3') != '') { ?>
           <?php echo minimalism_theme_front_block(theme_get_setting('minimalism_group3')); ?>
        <?php } else { ?>
         <?php print render($page['group3']); ?>
        <?php } ?>
   	  </section>
	</div>
	<!--end holder-->
   <!--start holder-->
   <div class="holder_content">
      <h3>Latest article</h3>
      <section class="group4">
       <?php if(theme_get_setting('article1') != '' || theme_get_setting('article2') != '' || theme_get_setting('article3') != '') { ?>
					 <?php if(theme_get_setting('article1') != ''){
             echo minimalism_theme_footer_block(theme_get_setting('article1'));
           }?>
           <?php if(theme_get_setting('article2') != ''){
             echo minimalism_theme_footer_block(theme_get_setting('article2'));
           }?>
           <?php if(theme_get_setting('article3') != ''){
             echo minimalism_theme_footer_block(theme_get_setting('article3'));
           }?>
           
       <?php } else { ?>
          <?php print render($page['before_footer']); ?>
       <?php } ?>    
       </section>
   </div>
   <!--end holder-->
   </div>
   <!--end container-->
   <!--start footer-->
   <footer>
      <div class="container-footer">  
			   <?php print render($page['footer']); ?>
      </div>
   </footer>
   <!--end footer-->
   </div>