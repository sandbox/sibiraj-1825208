<?php

/**
 * @file
 * Theme setting callbacks for the minimalism theme.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function minimalism_form_system_theme_settings_alter(&$form, &$form_state) {
 $form['minimalism_wrapper'] = array(
  '#type' => 'fieldset',
	'#title' => t('Configure your home page (Optional Settings)'),
	'#collapsible' => TRUE,
	'#collapsed' => FALSE,
 );
 $form['minimalism_wrapper']['intro'] = array(
  '#type' => 'fieldset',
	'#title' => t('Introduction Title and description'),
	'#collapsible' => TRUE,
	'#collapsed' => FALSE,
 );
 $form['minimalism_wrapper']['intro']['intro_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Introduction title'),
		'#default_value' => theme_get_setting('intro_title'),
  );
	$form['minimalism_wrapper']['intro']['intro_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Introduction description'),
		'#default_value' => theme_get_setting('intro_desc'),
  );
 $form['minimalism_wrapper']['intro']['intro_option'] = array(
    '#type' => 'select',
    '#title' => t('Options'),
		'#options' => array(0 => t('Disable'),1 => t('Front page only'),2 => t('All Pages')),
		'#default_value' => theme_get_setting('intro_option'),
  );
 $form['minimalism_wrapper']['group'] = array(
  '#type' => 'fieldset',
	'#description' => 'Enter any node id to configure this blocks. Eg :- 10',
	'#title' => t('Front page groups'),
	'#collapsible' => TRUE,
	'#collapsed' => FALSE,
 );
 $form['minimalism_wrapper']['group']['minimalism_group1'] = array(
    '#type' => 'textfield',
    '#title' => t('Group One'),
		'#default_value' => theme_get_setting('minimalism_group1'),
  );
 $form['minimalism_wrapper']['group']['minimalism_group2'] = array(
    '#type' => 'textfield',
    '#title' => t('Group Two'),
		'#default_value' => theme_get_setting('minimalism_group2'),
  );
 $form['minimalism_wrapper']['group']['minimalism_group3'] = array(
    '#type' => 'textfield',
    '#title' => t('Group Three'),
		'#default_value' => theme_get_setting('minimalism_group3'),
  );	
  $form['minimalism_wrapper']['footer_articles'] = array(
		'#type' => 'fieldset',
		'#title' => t('Footer Articles'),
		'#description' => 'Enter any node id to configure this blocks. Eg :- 10',
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	 );
 $form['minimalism_wrapper']['footer_articles']['article1'] = array(
    '#type' => 'textfield',
    '#title' => t('Footer Session one'),
		'#default_value' => theme_get_setting('article1'),
  );
 $form['minimalism_wrapper']['footer_articles']['article2'] = array(
    '#type' => 'textfield',
    '#title' => t('Footer Session two'),
		'#default_value' => theme_get_setting('article2'),
  );
 $form['minimalism_wrapper']['footer_articles']['article3'] = array(
    '#type' => 'textfield',
    '#title' => t('Footer Session three'),
		'#default_value' => theme_get_setting('article3'),
  );
}